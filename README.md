# back - rust - actix-web - posgres

* $ cargo --vesion
* $ cargo new rust_api_back_front --bin

## check code
----------------------
* $ cargo check

## performance test
- wrk =>  a web url performance test tool 
* $ wrk -t8 -c256 -d30s http://127.0.0.1:8081/

## endpoint
- fetch all data form postgres
- http://127.0.0.1:8081/get_all

- http://95.216.171.46:8081/get_all


## postgres
* $ sudo su - postgres
* $ psql

* CREATE USER test WITH password '2525_ap'
* CREATE DATABASE rust_yew WITH OWNER test
* GRANT ALL PRIVILEGES ON DATABASE rust_yew TO test
* DROP DATABASE rust_yew

 * $ psql -h 127.0.0.1 -p 5432 -U test -d rust_yew

## logging
* $ export RUST_LOG=info


