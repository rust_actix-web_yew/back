use crate::{db, models::InsertEmployee};
use crate::errors::MyError;
use crate::models::{Employee, ResponseData};

use actix_web::{web, HttpResponse, Responder};
use deadpool_postgres::{Client, Pool};
//use serde::{Deserialize, Serialize};
use tokio_pg_mapper::FromTokioPostgresRow;

use log::info;


pub async fn post_one(
    db_pool: web::Data<Pool>,
    items: web::Json<InsertEmployee>,
) -> impl Responder {
    let client: Client = db_pool
        .get()
        .await
        .expect("Error connecting to the database");
    let result = post_one_record(&client, items).await;
    info!("result, returns OK([]): {:#?}", result);

    match result {
        Ok(todos) => {               
            let data = ResponseData { 
                data: todos,
                message: "Success".to_string()
            };            
            HttpResponse::Ok().json(data)
        },
        Err(err) => {
            eprintln!("Error getting database names: {:?}", err);            
            //HttpResponse::InternalServerError().finish()
            HttpResponse::InternalServerError().json(ResponseData {             
                data: vec![],
                message: format!("Error: {:?}", err)
            })
        },
    }
}
 

pub async fn post_one_record(
    client: &Client,
    items: web::Json<InsertEmployee>,
) -> Result<Vec<Employee>, MyError> {
    let name = &items.name;    
    let email = &items.email;    
    let item = InsertEmployee {
        name: name.into(),        
        email: email.into(),        
    };
    
    let value = serde_json::to_string(&item);
    info!("value: {:#?}", value);
    info!("json: {:#?}", items);
    //println!("name: {:#?}", item.name);
        
    client.execute(
            "INSERT INTO tbemployee (name, email) VALUES ($1, $2)",
            &[&item.name, &item.email],
        )
        .await?;

    // fetch data
    let res = client
        .query("SELECT * FROM tbemployee WHERE name = $1 AND email = $2", &[&item.name, &item.email])
        .await?
        .iter()
        .map(|row| Employee::from_row_ref(row).unwrap())
        .collect::<Vec<Employee>>();

    Ok(res)
}


pub async fn get_all_records(db_pool: web::Data<Pool>) -> impl Responder {
    let client: Client = db_pool
        .get()
        .await
        .expect("Error connecting to the database");

    let result = db::get_todos(&client).await;    

    info!("result: {:#?}", result);

    match result {
        Ok(todos) =>  {
            // info!("result: {:#?}", todos);
            HttpResponse::Ok().json(todos)
        }, 
        Err(_) => HttpResponse::InternalServerError().into(),
    }
}
