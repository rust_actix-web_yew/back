use serde::{Deserialize, Serialize};
extern crate tokio_pg_mapper;
extern crate tokio_pg_mapper_derive;
use tokio_pg_mapper_derive::PostgresMapper;


#[derive(Debug, Serialize, Deserialize, PostgresMapper, Clone)]
#[pg_mapper(table = "tbemployee")]
pub struct Employee {
    pub id: i32,
    pub name: String,    
    pub email: String,        
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct InsertEmployee {
    pub name: String,    
    pub email: String,    
}

//expected struct `Vec<std::string::String>`
//   found struct `Vec<tokio_postgres::Row>`r


#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ResponseData {
    pub data: Vec<Employee>,        
    pub message: String,
}