
#!/usr/bin/env bash

cd /home/rust_yew/back

if lsof -Pi :8081 -sTCP:LISTEN -t >/dev/null ; then
    echo "Port 8081 is in use. Killing the process..."
    killall -9 $(lsof -t -i:8080)
else
    echo "Port 8081 is not in use."
fi
